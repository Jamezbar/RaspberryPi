from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from gpiozero import LED, Button
import datetime

def writefile(Bn,Bt,Bd):
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
        
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()

    values = [[Bn,Bt,Bd]]
    resource = {
        "majorDimension": "ROWS",
        "values" : values
    }
    spreadsheetId = "1ljYqD1dRCJ9N9szEooQLT6ZshFKX6G3OHmo7BWqaeRI"
    rang = "A:B"
    sheet.values().append(spreadsheetId=spreadsheetId, range = rang, body = resource, valueInputOption = "USER_ENTERED").execute()

def main():
    led = LED("17")
    b1 = Button("4")
    b2 = Button("23")
    b3 = Button("13")
    b4 = Button("21")

    while True:
        if b1.is_pressed:
            b1.when_pressed = led.on
            t = datetime.datetime.now()
            strTime = t.strftime("%m/%d/%Y")
            strDate = t.strftime("%H:%M:%S")
            try:
                writefile("ON", strTime,strDate)
            except:
                f = open("failedWrites.csv","a")
                f. write("ON;" + strTime + ";" + strDate + " \n")
                f.close()
            b1.when_released = led.off
        if b2.is_pressed:
            b2.when_pressed = led.on
            t = datetime.datetime.now()
            strTime = t.strftime("%m/%d/%Y")
            strDate = t.strftime("%H:%M:%S")
            try:
                writefile("CLEAN", strTime,strDate)
            except:
                f = open("failedWrites.csv","a")
                f. write("CLEAN;" + strTime + ";" + strDate + " \n")
                f.close()
            b2.when_released = led.off
        if b3.is_pressed:
            b3.when_pressed = led.on
            t = datetime.datetime.now()
            strTime = t.strftime("%m/%d/%Y")
            strDate = t.strftime("%H:%M:%S")
            try:
                writefile("GREEN", strTime,strDate)
            except:
                f = open("failedWrites.csv","a")
                f. write("GREEN;" + strTime + ";" + strDate + " \n")
                f.close()
            b3.when_released = led.off
        if b4.is_pressed:
            b4.when_pressed = led.on
            t = datetime.datetime.now()
            strTime = t.strftime("%m/%d/%Y")
            strDate = t.strftime("%H:%M:%S")
            try:
                writefile("BROWN", strTime,strDate)
            except:
                f = open("failedWrites.csv","a")
                f. write("BROWN;" + strTime + ";" + strDate + " \n")
                f.close()
            b4.when_released = led.off

if __name__ == '__main__':
    main()
