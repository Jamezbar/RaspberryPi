from gpiozero import DigitalInputDevice #generic sharp endge input on gpio
import datetime #gts date time
from datetime import timedelta #returns difference in time in better format
import smtplib #email protocol
import ssl #Securty protocol for sending email
from email.mime.multipart import MIMEMultipart #used to create a more readable email and add atachments
from email.mime.text import MIMEText #used to create a more readable email and add atachments
from email.mime.base import MIMEBase #used to create a more readable email and add atachments
from email import encoders #used to create a more readable email and add atachments
#List of imports
# another comment

switch = DigitalInputDevice(20) #the pin the reed switch is connected to
port = 465 #outgoing email port
pword = "iamjames23" #password for email address

#Initiate which switch is being listened on
subject = "Door Opened"
sender = "polreggroupdev@gmail.com"
recep = "jb.jbarclay.jb@gmail.com"
while True:
    switch.wait_for_inactive #pauses program until switch is disengaged(door being opened)
    t1 = datetime.datetime.now() #gets time of when door opens
    switch.wait_for_active #pauses program until switch is sengaged(door being closed)
    t2 = datetime.datetime.now() #gets tim of when the door closes
    td = t2-t1 #calculates time open
    tOpen = str(td)[:-7] #formats time open to string
    t1 = str(t1)[:-7] #formats time open to string
    t2 = str(t2)[:-7] #formats time open to string
    msg = "\nDoor was opened at: " + t1 + "\nDoor was closed at: " + t2 + "\nDoor was open for: " + tOpen #message to send
    print(msg) #testing print 
    f = open("OpenTime.csv", "a") #sets csv file as append
    f.write(t1 + ";" + t2 + ";" + tOpen + "\n") #writes time opened, time closed and time open
    f.close() #closes and saves the file
    
    message = MIMEMultipart() #setting up the email
    message["From"] = sender #the email address to send the  enail
    message["To"] = recep #the email address recieving the email(can be multiple emails)
    message["Subject"] = subject #subject of the email
    message.attach(MIMEText(msg, "plain")) #adding the body of the email
    with open(f.name , "rb") as attachment: # adding the attachment to the email
        part = MIMEBase("application", "vnd.ms-excel") #telling the email provider what to  do with the file
        part.set_payload(attachment.read()) #adding the file
    encoders.encode_base64(part) #encode file to email format
    part.add_header( #encode file to email format
        "Content-Disposition",#encode file to email format
        f"attachment; filename= {f}",#encode file to email format
    )
    message.attach(part) #attach encoded file to email
    text = message.as_string() #make message a string for the smtp email protocol

    context = ssl.create_default_context() #creates the default email sending protocol

#    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server: #sets up which email server to use
#        server.login("polreggroupdev@gmail.com", pword) #logs into the email
#        server.sendmail("polreggroupdev@gmail.com","jb.jbarclay.jb@gmail.com", text) #sends the mail from first email to second
#        print("sent") #testing print
#waits for door to open, checks time, waits for door to close, checks time, works out time delta, 
#writes to file/sends notificationmaybe send csv file as attachment as well each time
