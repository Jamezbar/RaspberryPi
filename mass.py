import serial #library for serial devices
import time #time library
import datetime #time library
from decimal import Decimal	#decimal library

ser1 = serial.Serial(port = 'placeholderPort',baudrate = 9600,parity = serial.PARITY_EVEN,stopbits = serial.STOPBITS_ONE,bytesize = serial.SEVENBITS,timeout = None)
ser2 = serial.Serial(port = 'placeholderPort',baudrate = 9600,parity = serial.PARITY_EVEN,stopbits = serial.STOPBITS_ONE,bytesize = serial.SEVENBITS,timeout = None)
ser3 = serial.Serial(port = 'placeholderPort',baudrate = 9600,parity = serial.PARITY_EVEN,stopbits = serial.STOPBITS_ONE,bytesize = serial.SEVENBITS,timeout = None)
#setting up which ports the scales are connected to
#new comment
tt = datetime.datetime.now() #time then
print(tt.second) #testing print
while True: #continuos loop
	x = ser1.readline() #read raw data from scale 1
	y = ser2.readline() #read raw data from scale 2
	z = ser3.readline() #read raw data from scale 3
	tn = datetime.datetime.now() #time now
	timediff = tn-tt #work out time difference
	check = round(timediff.total_seconds()/3600,3) #convert time to seconds ad work out time passed
	print(check) #testing print
	if(check == 1): #check if time passed is 1 hour
		tt = tn #set time then to now
		x = x.decode("utf8") #decode information to text from scale 1
		y = y.decode("utf8") #decode information to text from scale 2
		z = z.decode("utf8") #decode information to text from scale 3	
		date = tt.strftime("%d/%m/%Y %H:%M:%S") #format date to string

		w1 = x.strip() #strip scale 1 data of extra spaces and info
		w2 = y.strip() #strip scale 2 data of extra spaces and info
		w3 = z.strip() #strip scale 3 data of extra spaces and info

		file = open("results.csv", 'a') #Open file to wrte to
		writeString = "\n" + date + ";" + w1 + ";" + w2 + ";" + w3 #string to write to the csv file
		print(writeString) # testing print
		file.write(writeString) #writing the string
		file.close() #closing and saving the file
