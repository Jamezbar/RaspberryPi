from gpiozero import DigitalInputDevice #generic sharp endge input on gpio

reed = DigitalInputDevice(18) #pin to read from
global flow # variable to track how often switch is activated
flow = 0

def waterAdd(): #method that runs when switch is activated
    global flow
    flow +=1
    print(flow)

while True:
    reed.wait_for_inactive
    waterAdd()
#runs waterAdd everytime switch activates

#will add a file writing part once I know how fast it reads the water meter.
#This is all theoretical at the moment nuntil I can test how the reed switch works.
#This will just incrament an amount everytime the reed switch is activated

